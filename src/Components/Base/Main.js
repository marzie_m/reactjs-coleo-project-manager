import styled from 'styled-components'
export const CenterContent = styled.div`
    display: block;
    width: calc(100vw - 520px);
    height: 100vh;
    overflow-y: auto;
    padding-left: 20px;
    padding-right: 20px;
`;
